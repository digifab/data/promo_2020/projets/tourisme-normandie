import time
import pandas as pd
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC


driver = webdriver.Chrome()
driver.get('https://www.tripadvisor.fr/Restaurants-g187179-Normandy.html')

time.sleep(2)


df_iso = pd.read_csv("iso.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, encoding='utf-8-sig')
# df_iso_pays = pd.read_csv("iso_pays.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, encoding='utf-8-sig')


div_ville = 0
div_liste = 0
ville = 0
div_resto = 0


df2 = pd.DataFrame()


#################################################################################################################
#                                                Fonction de clic                                               #
#################################################################################################################

def clic_ville():
    global longueur_liste_ville
    try:
        bouton_ville = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'geo_entry')))
        longueur_liste_ville = len(bouton_ville)
        bouton_ville[div_ville].click()
    except:
        print('Impossible de cliquer sur la ville')


def clic_resto():
    global longueur_liste_resto
    bouton_resto = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.CLASS_NAME, '_2KPNYP9B ')))
    longueur_liste_resto = len(bouton_resto)
    bouton_resto[div_resto].click()
    driver.switch_to.window(driver.window_handles[1])


def page_suivante():
    suivant_resto = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.LINK_TEXT, 'Suivant')))
    suivant_resto.click()
        

def langue_click():
    try:
        langue = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//div[@data-tracker="All languages"]')))
        langue.click()
    except:
        print('impossible de cliquer sur toutes les langues')


def plus():
    try:
        plus = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//span[@class="taLnk ulBlueLinks"]')))
        plus.click()
    except:
        print('impossible de cliquer sur plus')


def suivant():
    suivant = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//*[@class="nav next ui_button primary  cx_brand_refresh_phase2"]')))
    suivant.click()


def precedent():
    precedent = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//*[@class="nav previous ui_button secondary  cx_brand_refresh_phase2"]')))
    precedent.click()

#####################################################################################################################################
#                                                Fonction de récupération des données                                               #
#####################################################################################################################################


def geo():
    if len(html) < 1:
        return geo

    # continent
    for i in html:
        try:
            continent = source.find_all('li', attrs={"class": 'breadcrumb'})
            continent = continent[0].text
            continent = continent.rstrip()
            dico['continent'].append(continent)
        except:
            dico['continent'].append('null')


    # nation
    for i in html:
        try:
            nation = source.find_all('li', attrs={"class": 'breadcrumb'})
            nation = nation[1].text
            nation = nation.rstrip()
            dico['nation'].append(nation)
        except:
            dico['nation'].append('null')


    # region
    for i in html:
        try:
            region = source.find_all('li', attrs={"class": 'breadcrumb'})
            region = region[2].text
            region = region.rstrip()
            dico['region'].append(region)
        except:
            dico['region'].append('null')


    # departement
    for i in html:
        try:
            departement = source.find_all('li', attrs={"class": 'breadcrumb'})
            departement = departement[4].text
            departement = departement.rstrip()
            dico['departement'].append(departement)
        except:
            dico['departement'].append('null')


    # iso 3166
    for i in html:
        try:
            comparaison = set(df_iso['Nom']) & set(dico['departement'])
            comparaison = list(comparaison)
            for i in comparaison:
                dpt_iso = (df_iso['Code'][df_iso['Nom'] == i])
            dpt_iso = str(dpt_iso)
            dpt_iso = dpt_iso[6:11]
            dico['dpt_iso'].append(dpt_iso)
        except:
            dico['dpt_iso'].append('null')   


    # commune
    for i in html:
        try:
            commune = source.find_all('li', attrs={"class": 'breadcrumb'})
            commune = commune[5].text
            commune = commune.rstrip()
            dico['commune'].append(commune)
        except:
            dico['commune'].append('null')


def type_act():
    if len(html) < 1:
        return type_act
    for i in html:
        try:
            dico['type_activite'].append('Restaurant')
        except:
            dico['type_activite'].append('null')


def note_globale():
    if len(html) < 1:
        return note_globale
    for i in html:
        try:
            bubble = source.find('span', attrs={"class": '_3YGVJGd7'})
            liste = [10, 15, 20, 25, 30, 35, 40, 45, 50]
            for i in liste:
                if 'ui_bubble_rating bubble_'+str(i) in str(bubble):
                    noteglob = i/10
            dico['note_globale'].append(noteglob)
        except:
            dico['note_globale'].append('null')


def nom_activite():
    if len(html) < 1:
        return nom_activite
    for i in html:
        try:
            nom_act = source.find('h1', attrs={"class": '_3a1XQ88S'})
            dico['nom_activite'].append(nom_act.text)
        except:
            dico['nom_activite'].append('null')


def nom():
    if len(html_nom) < 1:
        return nom    
    for i in html_nom:
        try:
            name = i.find('div', attrs={"class": 'info_text pointer_cursor'})
            dico['nom'].append(name.text)
        except:
            dico['nom'].append('null')


def commentaire():
    if len(html) < 1:
        return commentaire
    for i in html:
        try:
            comm = i.find('p', attrs={"class": 'partial_entry'})
            dico['commentaire'].append(comm.text)
        except:
            dico['commentaire'].append('null')


def pays_user():
    if len(html) < 1:
        return pays_user
    for i in html:
        try:
            origine = i.find('div', attrs={"class": 'userLoc'})
            origine = origine.text
            # Isolation de l'élément contenant le pays
            ville, pays = origine.split(', ')
            pays = pays.strip()
            dico['ville'].append(ville)
            dico['pays'].append(pays)
        except:
            dico['pays'].append('null')
            dico['ville'].append('null')


# def iso_pays():
#     for i in html:
#         try:
#             comparaison = set(df_iso_pays['Name']) & set(dico['pays'])
#             comparaison = list(comparaison)
#             for i in comparaison:
#                 iso_pays = (df_iso_pays['Code'][df_iso_pays['Name'] == i])
#             iso_pays = str(iso_pays)
#             iso_pays = iso_pays[6:9]
#             iso_pays = iso_pays.strip()
#             dico['iso_pays'].append(iso_pays)
#         except:
#             dico['iso_pays'].append('null')


def note():
    if len(html) < 1:
        return note
    for i in html:
        try:
            bubble = i.find('div', attrs={"class": 'ui_column is-9'})
            bubble = str(bubble).replace('<div class="ui_column is-9"><span class="ui_bubble_rating bubble_', '')
            bubble = bubble[:bubble.find('"></span><span class="ratingDate"')]
            bubble = float(bubble)/10
            dico['note'].append(bubble)
        except:
            dico['note'].append('null')


def periode():
    if len(html) < 1:
        return periode
    for i in html:
        try:
            date = i.find('div', attrs={"class": 'prw_rup prw_reviews_stay_date_hsx'})
            date = date.text
            date = date.replace("Date de la visite", '').replace(': ', '')
            date = date.replace(date[0], '')
            dico['periode'].append(date)
        except:
            dico['periode'].append('null')



def type_cuisine():
    if len(html) < 1:
        return type_cuisine
    for i in html:
        try:
            cuisine = source.find_all('a', attrs={"class": '_2mn01bsa'})
            dico['type_cuisine'].append(cuisine[1].text)
        except:
            dico['type_cuisine'].append('null')    


def lat_long():
    if len(html) < 1:
        return lat_long
    lat = []
    lng = []

    f = open("geo_resto.txt", "r", encoding='utf8').read()
    try:
        i_lat = f.find('""latitude"":""') # renvoie l'index où se trouve le premier caractère du find
        i_lng = f.find('"",""longitude"":""')
        for j in range(i_lat+15, i_lng):
            lat.append(str(f[j]))

        lat = ''.join(str(e) for e in lat) # Conversion de la liste en string
        lat = lat.replace(',', '')
        for i in html:
            dico['latitude_commune'].append(float(lat))
    except:
        for i in html:
            dico['latitude_commune'].append('null')

    try:
        i_lng = f.find('""longitude"":""')
        last_index = f.find('"",""num_reviews""')
        for j in range(i_lng+16, last_index):
            lng.append(str(f[j]))

        lng = ''.join(str(e) for e in lng)
        lng = lng.replace(',', '')
        for i in html:
            dico['longitude_commune'].append(float(lng))
    except:
        for i in html:
            dico['longitude_commune'].append('null')



def scraping_resto():
    global dico, df2, html, source, html_nom
    avis = 10

    while True:
        source2 = driver.page_source
        source2 = BeautifulSoup(source2, 'html.parser')
        # s'il n'y a pas de commentaire, passe au resto suivant
        try:
            html_nom = source2.find_all('div', attrs={"class": 'review-container'})
        except:
            return scraping_resto

        dico = {'continent': [], 'nation': [], 'region': [], 'departement': [], 'dpt_iso': [], 'commune': [], 'latitude_commune': [], 'longitude_commune': [], 'nom_activite': [], 'type_cuisine': [], 'type_activite': [], 'note_globale': [], 'nom': [], 'commentaire': [], 'pays': [], 'ville': [], 'note': [], 'periode': []}
        nom()
        time.sleep(2)
        plus()
        time.sleep(1)
        source = driver.page_source
        source = BeautifulSoup(source, 'html.parser')
        df = pd.DataFrame(data=source)
        df.to_csv(r'geo_resto.txt', index=False)
        html = source.find_all('div', attrs={"class": 'review-container'})
        
        geo()
        lat_long()
        type_act()
        nom_activite()
        note_globale()
        commentaire()
        pays_user()
        # iso_pays()
        note()
        periode()
        type_cuisine()

        df = pd.DataFrame(dico)
        df2 = df2.append(df)
        # df.to_csv("restaurant.csv", mode='a', sep=';', index=False, header=False, encoding='utf-8-sig')
        print(df2)

        log = 'ville '+str(ville)+'/191'+'\n'+'page '+str(page_actuelle_resto)+'/'+str(resto_nb_page)+'\n'+'restaurant '+str(div_resto+1)+'/'+str(longueur_liste_resto)+'\n'+'avis '+str(avis)+'/'+str(count_avis)+'\n'+'div '+str(div_liste)+'\n'+'url : '+log_url
        print(log)
        with open("log_restaurant.txt", "w") as f:
            f.write(log)

        avis += len(df.index)

        
        try:
            suivant()
            time.sleep(2)
        except:
            print('dernière page de commentaire')
            return scraping_resto
              
        


def parcours_resto():
    global div_resto, count_avis, resto_nb_page, page_actuelle_resto
    page_actuelle_resto = 1

    # Permet de voir le nombre de page de restaurant restant
    try:
        resto_nb_page = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.XPATH, '//a[@class="pageNum taLnk"]')))[-1].text
    except:
        try:
            resto_nb_page = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, '//a[@class="pageNum taLnk"]'))).text
        except:
            resto_nb_page = 1

    while True:
        try:
            clic_resto()
            time.sleep(2)
        except:
            div_resto = 0
            page_suivante()
            time.sleep(2)
            clic_resto

        # Permet de voir le nombre d'avis restant
        try:
            count_avis = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, '//span[@class="_3Wub8auF"]'))).text
            count_avis = count_avis.replace(' avis', '')
            count_avis = int(count_avis)
        except:
            pass

        langue_click()
        time.sleep(2)

        scraping_resto()
        time.sleep(2)

        driver.close()
        driver.switch_to.window(driver.window_handles[0])
        time.sleep(2)

        div_resto += 1
        if div_resto == longueur_liste_resto:
            div_resto = 0
            page_actuelle_resto += 1
            try:
                page_suivante()
                time.sleep(2)
            except:
                print('fin des pages')
                page_actuelle_resto = 1
                return parcours_resto


# Mettre en commentaire la boucle ci-dessous s'il faut reprendre le scraper après la première page
while True:
   log_url = driver.current_url
   clic_ville()
   ville += 1
   parcours_resto()
   driver.back()
   time.sleep(3)
   div_ville += 1
   if div_ville == longueur_liste_ville:
       div_ville = 0
       break

page_suivante()
time.sleep(5)


##########################################################################################################################################################
# A partir de la deuxième page, la liste n'a plus la même structure donc il faut redéfinir la fonction clic_ville, page_suivante  et reprendre la boucle #
##########################################################################################################################################################



def page_suivante2():
    try:
        suivant_resto = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//a[@class="guiArw sprite-pageNext  pid0"]')))
        suivant_resto.click()
    except:
        suivant_resto = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//a[@class="uiArw sprite-pageNext  pid0"]')))
        suivant_resto.click()        


def clic_ville2():
    global longueur_liste_ville2
    try:       
        nombre_ville = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.XPATH, '//span[@class="state"]')))
        longueur_liste_ville2 = len(nombre_ville)
        bouton_ville = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, '//*[@id="LOCATION_LIST"]/ul/li['+str(div_liste)+']/a')))
        bouton_ville.click()
    except:
        print('Impossible de cliquer sur la ville')


while True:
    log_url = driver.current_url
    clic_ville2()
    ville += 1
    parcours_resto()
    driver.back()
    time.sleep(3)
    div_liste += 1
    if div_liste == longueur_liste_ville2 + 1:
        div_liste = 1
        try:
            page_suivante2()
        except:
            print('Fin des pages')
            break


driver.quit()
