import time
import pandas as pd
import collections
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC


DIGIT = 0
div = 3

driver = webdriver.Chrome()
personne = {'nom_activite': [], 'type_activite': [], 'note_global': [],'nom': [], 'pays': [], 'ville': [], 'commentaire': [], 'periode': [], 'note': []}

url = 'https://www.tripadvisor.fr/Attractions-g187179-Activities-c47-Normandy.html'
driver.get(url)

def clic_site(div=None):
    bouton_site = WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="component_7"]/div/div[2]/div['+str(div)+']/div/div[2]/a[1]/h2')))
    global pdnom_activite, pdtype_activite
    pdnom_activite = bouton_site.text
    pdtype_activite = 'Site touristique'
    bouton_site.click()
    driver.switch_to_window(driver.window_handles[1])
    return pdnom_activite, pdtype_activite


def scrap_note():
    global pdnote_global
    driver.switch_to_window(driver.window_handles[1])
    url = driver.current_url
    content = requests.get(url)
    content = BeautifulSoup(content.text, 'html.parser')
    site = content.find('div', attrs={"class": '_4AHMMRq5'})
    liste = [10, 15, 20, 25, 30, 35, 40, 45, 50]
    for i in liste:
        if 'ui_bubble_rating bubble_'+str(i) in str(site):
            pdnote_global = i/10
    return pdnote_global


def pagesuivante(DIGIT):
    link = 'https://www.tripadvisor.fr/Attractions-g187179-Activities-c47-oa'+str(DIGIT)+'-Normandy.html'
    driver.get(link)
    return pagesuivante


def scrap_personne():

    def langue_click():
        try:
            langue = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//li[@class="ui_radio _3gEj_Jb5 _2SqbCAu5"]')))
            langue.click()
        except:
            print('impossible de cliquer sur la langue')
            langue = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//a[@class="_1bhK4ZzT _1GMuaJ-3 _a46Fuh9"]')))
            langue.click()

    def note_personne():
        source_html = driver.page_source
        source_html = BeautifulSoup(source_html, 'html.parser')
        source_html = source_html.find_all('div', attrs={"class": 'nf9vGX55'})
        for m in source_html:
            m = str(m).replace('<div class="nf9vGX55" data-test-target="review-rating"><span class="ui_bubble_rating bubble_', '').replace('"></span></div>', '')
            m = float(m)/10
            personne['note'].append(m)


    def commentaire():
        bloc = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.XPATH, '//q[@class="IRsGHoPm"]')))
        if len(bloc) < 5:
            driver.close()
            driver.switch_to.window(driver.window_handles[0])
            pass
        for j in bloc:
            personne['commentaire'].append(j.text)
        

    def periode():
        periode = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.XPATH, '//span[@class="_34Xs-BQm"]')))
        longueur = 5 - len(periode)
        for k in periode:
            k = k.text
            k = k.replace("Date de l'expérience : ", '').replace(' ', '-')
            personne['periode'].append(k)
        if longueur != 0:
            while longueur != 0:
                personne['periode'].append('null')
                longueur -= 1


    def nation_nom():
        langue_click()
        time.sleep(1)
        try:
            note_personne()
        except Exception as E:
            print(E)
            print("impossible de scrapper la note")
        time.sleep(2)
        plus = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, 'XUVJZtom')))
        plus.click()
        try:
            commentaire()
        except Exception as E:
            print(E)
            print("impossible de scrapper les commentaire")
        try:
            periode()
        except Exception as E:
            print(E)
            print("impossible de scrapper la periode")
        div_block = [3, 4, 5, 6, 7]
        for i in div_block:
            block = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.XPATH, '//*[@id="component_19"]/div[3]/div/div['+str(i)+']/div[1]/a')))
            for s in block:
                personne['nom_activite'].append(pdnom_activite)
                personne['type_activite'].append(pdtype_activite)
                personne['note_global'].append(pdnote_global)
                time.sleep(2)
                try:
                    try:
                        ActionChains(driver).move_to_element(s).key_down(Keys.CONTROL).click(s).key_up(Keys.CONTROL).perform()
                        driver.switch_to.window(driver.window_handles[2])
                        nation = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, '_1yMDrJ5H')))
                        nation = nation.text.replace('\n', '')
                        head, sep, tail = nation.partition('Membre') # Supprime tout ce qu'il y a après "Membre"
                    except:
                        print('impossible de cliquer sur le profil')
                    try:
                        ville, pays = head.split(', ')
                        personne['ville'].append(ville)
                        personne['pays'].append(pays)
                    except:
                        personne['ville'].append('null')
                        personne['pays'].append('null')
                    try:
                        nom = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, '//span[@class="_2wpJPTNc _345JQp5A"]')))
                        personne['nom'].append(nom.text)
                    except:
                        personne['nom'].append('null')
                except:
                    personne['ville'].append('null')
                    personne['pays'].append('null')
                    personne['nom'].append('null')
                driver.close()
                driver.switch_to.window(driver.window_handles[1])
                time.sleep(2)
        difference = len(personne['commentaire']) - len(personne['ville'])
        if difference != 0:
            while difference != 0:
                personne['nom_activite'].append('null')
                personne['type_activite'].append('null')
                personne['note_global'].append('null')
                personne['nom'].append('null')
                personne['pays'].append('null')
                personne['ville'].append('null')
                difference -= 1
        df2 = pd.DataFrame.from_dict(personne)
        df2.to_csv('personne.csv', sep=';', index=False, header=True, encoding='utf-8-sig')
        print(df2)
        try:
            suivant = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//*[@class="ui_button nav next primary "]'))) 
            suivant.click()
        except Exception as E:
            print(E)
            print('dernière page')
        time.sleep(2)
    nation_nom()


def scrapping():
    try:
        clic_site(div)
    except Exception as E:
        print(E)
        print('impossible de cliquer sur le site')
        return scrapping
    scrap_note()
    while True:
        scrap_personne()
    return scrapping


while True:
    try:
        scrapping()
        div += 1
        if div == 32 or div > 32:
            div = 3
            DIGIT += 30
            driver.close()
            driver.switch_to.window(driver.window_handles[0])
            pagesuivante(DIGIT)
        else:
            driver.close()
            driver.switch_to.window(driver.window_handles[0])
            time.sleep(5)
    except Exception as E:
        div += 1
        if div == 32 or div > 32:
            div = 3
            DIGIT += 30
            driver.close()
            driver.switch_to.window(driver.window_handles[0])
            pagesuivante(DIGIT)
        else:
            print(E)
        continue
