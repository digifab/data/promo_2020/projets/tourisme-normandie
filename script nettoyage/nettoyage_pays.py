import pandas as pd
import pycountry
from googletrans import Translator


df = pd.read_csv("final.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, low_memory=False)

total_row = len(df.index)

translator = Translator()

pays = ['Russie', 'États-Unis', 'Vietnam']
pays_csv = df['pays'].tolist()
df_list = []

print('Ajout à la liste')
for i in pycountry.countries:
    pays.append(translator.translate(i.name, dest='fr').text)

x=1
for i in pays_csv:
    if i not in pays:
        df_list.append('null')
    else:
        df_list.append(i)
            
    print(str(x)+'/'+str(total_row))
    x += 1



df['pays'] = df_list
print(df)
print(df.dtypes)

df.to_csv("final.csv", sep=';', index=False, header=True, encoding='utf-8-sig')