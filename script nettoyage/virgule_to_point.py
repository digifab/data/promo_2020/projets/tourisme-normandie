import pandas as pd

df = pd.read_csv("hotel_site_out.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, low_memory=False)

total_row = len(df.index)

list_note = df['longitude_commune'].tolist()
new_liste = []

x=1

for i in list_note:
    i = str(i).replace(',', '.')
    new_liste.append(float(i))

    print(str(x)+'/'+str(total_row))
    x += 1


df['longitude_commune'] = new_liste
print(df)

df.to_csv("hotel_site_out.csv", sep=';', index=False, header=True, encoding='utf-8-sig')