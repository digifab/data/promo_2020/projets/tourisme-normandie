import pandas as pd


df = pd.read_csv("hotel_site_out.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, low_memory=False)
df2 = pd.read_csv("restaurant_final.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, low_memory=False)

frames = [df, df2]

df3 = pd.concat(frames)

df3.to_csv("final.csv", sep=';', index=False, header=True, encoding='utf-8-sig')
print(df3)
print(df3.dtypes)