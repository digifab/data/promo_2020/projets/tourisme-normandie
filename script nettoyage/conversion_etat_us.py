# script de remplacement
import pandas as pd


df = pd.read_csv("final.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, low_memory=False)
print(df.index)

total_row = len(df.index)

etat_us = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'Californie', 'Caroline du Nord', 'Caroline du Sud', 'Colorado', 'Connecticut', 'Dakota du Nord', 'Dakota du Sud', 'Delaware', 'Floride', 'Géorgie', 'Hawaï', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiane', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'État de New York', 'Nouveau-Mexique', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvanie', 'Rhode Island', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginie', 'Virginie-Occidentale', 'Washington', 'Wisconsin', 'Wyoming', 'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming']


for i in etat_us:
    df['pays'] = df['pays'].replace([i], 'États-Unis')


print(df)

df.to_csv("final.csv", sep=';', index=False, header=True, encoding='utf-8-sig')