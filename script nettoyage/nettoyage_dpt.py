import pandas as pd

df = pd.read_csv("final.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, low_memory=False)

total_row = len(df.index)


dpt = ['Manche', 'Calvados', 'Seine-Maritime', 'Eure', 'Orne']
dpt_csv = df['departement'].tolist()

df_list = []


x=1
for i in dpt_csv:
    if i not in dpt:
        df_list.append('null')
    else:
        df_list.append(i)
            
    print(str(x)+'/'+str(total_row))
    x += 1



df['departement'] = df_list
print(df)

df.to_csv("final.csv", sep=';', index=False, header=True, encoding='utf-8-sig')