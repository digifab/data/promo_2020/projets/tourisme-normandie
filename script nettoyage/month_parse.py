import pandas as pd


df = pd.read_csv("restaurant_final.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, low_memory=False)

total_row = len(df.index)

# conversion de la colonne en datetime pour la conversion des mois en chiffre
df['periode']= pd.to_datetime(df['periode'])


# conversion des mois en chiffre
df['mois'] = df['periode'].dt.month

df['mois'].fillna('0', inplace=True)

# il faut que la colonne ai un type int64 pour la comparaison de la liste et de cette colonne
df['mois'] = df['mois'].astype(int)

dico_mois = {'0': 'null', '1' : 'Janvier', '2' : 'Février', '3' : 'Mars', '4' : 'Avril', '5' : 'Mai', '6' : 'Juin', '7' : 'Juillet', '8' : 'Aout', '9' : 'Septembre', '10' : 'Octobre', '11' : 'Novembre', '12': 'Décembre'}


liste_mois = []

x = 1
for i in df['mois']:
    for j in dico_mois:
        if str(i) == j:
            liste_mois.append(dico_mois[j])
    print(str(x)+'/'+str(total_row))
    x += 1


df['mois'] = liste_mois
print(df)
df.to_csv("restaurant_final.csv", sep=';', index=False, header=True, encoding='utf-8-sig')