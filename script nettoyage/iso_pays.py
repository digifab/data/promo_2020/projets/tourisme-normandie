# script d'ajout de nouvelle colonne avec comparaison
# lancer le script de nettoyage de pays avant

import pandas as pd

df_iso = pd.read_csv("iso_pays.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, low_memory=False)
df = pd.read_csv("final.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, low_memory=False)


total_row = len(df.index)


dict_iso = df_iso.set_index('Name').T.to_dict('list')



liste_iso = []

x = 1
for i in df['pays']:
    if str(i) == 'nan':
        liste_iso.append('null')
    else:
        for j in dict_iso:
            if str(i) == j:
                liste_iso.append(dict_iso[j][0])
    print(str(x)+'/'+str(total_row))
    x += 1



df['iso_pays'] = liste_iso

print(df)

df.to_csv("final.csv", sep=';', index=False, header=True, encoding='utf-8-sig')