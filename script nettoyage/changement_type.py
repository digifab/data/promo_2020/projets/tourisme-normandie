import pandas as pd

df = pd.read_csv("final.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, low_memory=False)

print(df.astype({'latitude_commune': 'float'}).dtypes)


# df.to_csv("final.csv", sep=';', index=False, header=True, encoding='utf-8-sig')