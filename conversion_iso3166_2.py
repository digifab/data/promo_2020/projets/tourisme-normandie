import pandas as pd


df_iso = pd.read_csv("iso.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, encoding='utf-8-sig')
df_scrap = pd.read_csv("scrap.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, encoding='utf-8-sig')


iso_3166 = []
df_column = []

x = 0
for i in df_scrap['departement']:
    value = df_iso['Code'][df_iso['Nom'] == i]
    df_column.append(str(value))
    print(str(x)+'/177460')
    x += 1


for i in df_column:
    i = i[6:11]
    iso_3166.append(i)


df_scrap['iso_dpt'] = iso_3166

df_scrap.to_csv("final.csv", sep=';', index=False, header=True, encoding='utf-8-sig')