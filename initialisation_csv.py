import pandas as pd

dico = {'continent': [], 'nation': [], 'region': [], 'departement': [], 'dpt_iso': [], 'commune': [], 'latitude_commune': [], 'longitude_commune': [], 'nom_activite': [], 'type_activite': [], 'note_globale': [], 'nom': [], 'commentaire': [], 'pays': [], 'ville': [], 'note': [], 'periode': []}
dico_resto = {'continent': [], 'nation': [], 'region': [], 'departement': [], 'dpt_iso': [], 'commune': [], 'latitude_commune': [], 'longitude_commune': [], 'nom_activite': [], 'type_cuisine': [], 'type_activite': [], 'note_globale': [], 'nom': [], 'commentaire': [], 'pays': [], 'ville': [], 'note': [], 'periode': []}

df = pd.DataFrame(dico)
df_resto = pd.DataFrame(dico_resto)

df.to_csv("site_touristique.csv", sep=';', index=False, header=True, encoding='utf-8-sig')
df.to_csv("hotel.csv", sep=';', index=False, header=True, encoding='utf-8-sig')
df_resto.to_csv("restaurant.csv", sep=';', index=False, header=True, encoding='utf-8-sig')