import time
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC


driver = webdriver.Chrome()
driver.get('https://www.tripadvisor.fr/Hotels-g187179-Normandy-Hotels.html')
time.sleep(2)

df_iso = pd.read_csv("iso.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, encoding='utf-8-sig')
# df_iso_pays = pd.read_csv("iso_pays.csv", delimiter=";", error_bad_lines=False, warn_bad_lines=False, encoding='utf-8-sig')

div = 1
DIGIT = 0
df2 = pd.DataFrame()


def geo():
    if len(html) < 5:
        return geo
    def continent():
        for i in html:
            try:
                continent = source.find_all('li', attrs={"class": 'breadcrumb'})
                continent = continent[0].text
                continent = continent.rstrip()
                dico['continent'].append(continent)
            except:
                dico['continent'].append('null')

    def nation():
        for i in html:
            try:
                nation = source.find_all('li', attrs={"class": 'breadcrumb'})
                nation = nation[1].text
                nation = nation.rstrip()
                dico['nation'].append(nation)
            except:
                dico['nation'].append('null')

    def region():
        for i in html:
            try:
                region = source.find_all('li', attrs={"class": 'breadcrumb'})
                region = region[2].text
                region = region.rstrip()
                dico['region'].append(region)
            except:
                dico['region'].append('null')

    def departement():
        for i in html:
            try:
                departement = source.find_all('li', attrs={"class": 'breadcrumb'})
                departement = departement[4].text
                departement = departement.rstrip()
                dico['departement'].append(departement)
            except:
                dico['departement'].append('null')

    def iso_3166():
        for i in html:
            try:
                comparaison = set(df_iso['Nom']) & set(dico['departement'])
                comparaison = list(comparaison)
                for i in comparaison:
                    dpt_iso = (df_iso['Code'][df_iso['Nom'] == i])
                dpt_iso = str(dpt_iso)
                dpt_iso = dpt_iso[6:11]
                dico['dpt_iso'].append(dpt_iso)
            except:
                dico['dpt_iso'].append('null')

    def commune():
        for i in html:
            try:
                commune = source.find_all('li', attrs={"class": 'breadcrumb'})
                commune = commune[5].text
                commune = commune.rstrip()
                dico['commune'].append(commune)
            except:
                dico['commune'].append('null')
    continent()
    nation()
    region()
    departement()
    iso_3166()
    commune()


def nom_activite():
    if len(html) < 5:
        return nom_activite
    for i in html:
        try:
            nom_act = source.find('h1', attrs={"class": '_1mTlpMC3'})
            dico['nom_activite'].append(nom_act.text)
        except:
            dico['nom_activite'].append('null')


def note_globale():
    if len(html) < 5:
        return note_globale
    for i in html:
        try:
            bubble = source.find('div', attrs={"class": '_2F5IkNIg'})
            liste = [10, 15, 20, 25, 30, 35, 40, 45, 50]
            for i in liste:
                if 'ui_bubble_rating bubble_'+str(i) in str(bubble):
                    noteglob = i/10
            dico['note_globale'].append(noteglob)
        except:
            dico['note_globale'].append('null')


def commentaire():
    if len(html) < 5:
        return commentaire
    for i in html:
        try:
            comm = i.find('q', attrs={"class": 'IRsGHoPm'})
            dico['commentaire'].append(comm.text)
        except:
            dico['commentaire'].append('null')


def nom():
    if len(html) < 5:
        return nom
    for i in html:
        try:
            name = i.find('a', attrs={"class": 'ui_header_link _1r_My98y'})
            dico['nom'].append(name.text)
        except:
            dico['nom'].append('null')


def pays_user():
    if len(html) < 5:
        return pays_user
    for i in html:
        try:
            origine = i.find('div', attrs={"class": '_1EpRX7o3'})
            origine = origine.text
            # Isolation de l'élément contenant le pays
            origine = origine.rsplit('contributions')
            origine = origine[0] # Plusieurs span dans la div, on selectionne le premier
            origine = ''.join([i for i in origine if not i.isdigit()])
            ville, pays = origine.split(', ')               
            pays = pays.strip()
            pays = pays.replace(' contribution', '').replace(' contribution vote utile', '').replace(' vote utile', '').replace(' votes utiles', '')
            dico['ville'].append(ville)
            dico['pays'].append(pays)
        except:
            dico['pays'].append('null')
            dico['ville'].append('null')                
        

# def iso_pays():
#     for i in html:
#         try:
#             comparaison = set(df_iso_pays['Name']) & set(dico['pays'])
#             comparaison = list(comparaison)
#             print(comparaison)
#             for i in comparaison:
#                 iso_pays = (df_iso_pays['Code'][df_iso_pays['Name'] == i])
#                 print(iso_pays)
#             iso_pays = str(iso_pays)
#             iso_pays = iso_pays[6:9]
#             iso_pays = iso_pays.strip()
#             dico['iso_pays'].append(iso_pays)
#         except:
#             dico['iso_pays'].append('null')


def note():
    if len(html) < 5:
        return note
    for i in html:
        try:
            bubble = i.find('div', attrs={"class": 'nf9vGX55'})
            bubble = str(bubble).replace('<div class="nf9vGX55" data-test-target="review-rating"><span class="ui_bubble_rating bubble_', '').replace('"></span></div>', '')
            bubble = float(bubble)/10
            dico['note'].append(bubble)
        except:
            dico['note'].append('null')


def periode():
    if len(html) < 5:
        return periode
    for i in html:
        try:
            date = i.find('span', attrs={"class": '_34Xs-BQm'})
            date = date.text
            date = date.replace("Date du séjour", '').replace(': ', '')
            date = date.replace(date[0], '')
            dico['periode'].append(date)
        except:
            dico['periode'].append('null')


def langue_click():
    try:
        langue = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//li[@class="ui_radio _3gEj_Jb5 _2SqbCAu5"]')))
        langue.click()
    except:
        print('impossible de cliquer sur la langue, fermeture du pop-up...')
        pop_up = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//button[@class="_3VKU_-kL"]')))
        pop_up.click()
        langue.click()

def plus():
    try:
        plus = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.CLASS_NAME, 'XUVJZtom')))
        plus.click()
    except:
        print('impossible de cliquer sur plus')


def clic_site():
    try:
        bouton_site = WebDriverWait(driver, 5).until(EC.presence_of_all_elements_located((By.CLASS_NAME, '_2p1dMDlc')))
        bouton_site[div].click()
        driver.switch_to.window(driver.window_handles[1])
    except Exception as E:
        print(E)



def type_act():
    if len(html) < 5:
        return type_act
    for i in html:
        try:
            dico['type_activite'].append('Hotel')
        except:
            dico['type_activite'].append('null')


def suivant():
    suivant = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, '//*[@class="ui_button nav next primary "]'))) 
    suivant.click()


def lat_long():
    if len(html) < 5:
        return lat_long
    lat = []
    lng = []

    f = open("geo_hotel.txt", "r", encoding='utf8').read()
    try:
        i_lat = f.find(',""latitude"":') # renvoie l'index où se trouve le premier caractère du find
        i_lng = f.find(',""longitude"":')
        for j in range(i_lat+14, i_lng): # selectionne ce qu'il y a entre après i_lat et avant i_lng
            lat.append(str(f[j]))

        lat = ''.join(str(e) for e in lat) # Conversion de la liste en string
        lat = lat.replace(',', '')
        for i in html:
            dico['latitude_commune'].append(float(lat))
    except:
        for i in html:
            dico['latitude_commune'].append('null')

    try:
        i_lng = f.find(',""longitude"":')
        last_index = f.find(',""linksForWebsite"":')
        for j in range(i_lng+15, last_index):
            lng.append(str(f[j]))

        lng = ''.join(str(e) for e in lng)
        lng = lng.replace(',', '')
        for i in html:
            dico['longitude_commune'].append(float(lng))
    except:
        for i in html:
            dico['longitude_commune'].append('null')


def scraping():
    while True:
        global dico
        dico = {'continent': [], 'nation': [], 'region': [], 'departement': [], 'dpt_iso': [], 'commune': [], 'latitude_commune': [], 'longitude_commune': [], 'nom_activite': [], 'type_activite': [], 'note_globale': [], 'nom': [], 'commentaire': [], 'pays': [], 'ville': [], 'note': [], 'periode': []}
        time.sleep(2)
        plus()
        time.sleep(1)
        global html, source
        source = driver.page_source
        source = BeautifulSoup(source, 'html.parser')
        df = pd.DataFrame(data=source)
        df.to_csv(r'geo_hotel.txt', index=False)
        html = source.find_all('div', attrs={"class": '_2wrUUKlw _3hFEdNs8'})
        geo()
        lat_long()
        type_act()
        nom_activite()
        note_globale()
        nom()
        commentaire()
        pays_user()
        # iso_pays()
        note()
        periode()
        try:
            suivant()
        except:
            print('dernière page')
            break
        driver.switch_to.window(driver.window_handles[0])
        time.sleep(1)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);") # Scroll pour éviter le refresh de la page
        time.sleep(1)
        driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.HOME)
        driver.switch_to.window(driver.window_handles[1])
        df = pd.DataFrame(dico)
        global df2
        df2 = df2.append(df)
        print(df2)
        df.to_csv("hotel.csv", mode='a', sep=';', index=False, header=False, encoding='utf-8-sig')
    return html, source


while True:
    clic_site()
    time.sleep(2)
    langue_click()
    scraping()
    driver.close()
    time.sleep(1)
    driver.switch_to.window(driver.window_handles[0])
    time.sleep(2)
    div += 1
    if div == 32:
        div = 1
        DIGIT += 30
        driver.get('https://www.tripadvisor.fr/Hotels-g187179-oa'+str(DIGIT)+'-Normandy-Hotels.html')
        time.sleep(5)

driver.quit()